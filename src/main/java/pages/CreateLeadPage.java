package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id = "createLeadForm_companyName") WebElement eleCname;
	@FindBy(id = "createLeadForm_firstName") WebElement eleFname;
	@FindBy(id= "createLeadForm_lastName") WebElement eleLname;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement eleClkCrtLead;
	
	public CreateLeadPage Cname(String data)
	{
		type(eleCname, data);
		return this;
	}
	
	public CreateLeadPage Fname(String data1)
	{
		type(eleFname, data1);
		return this;
	}
	
	public CreateLeadPage Lname(String data2)
	{
		type(eleLname, data2);
		return this;
	}
	
	public ViewLeadPage CleadSub()
	{
		click(eleClkCrtLead);
		return new ViewLeadPage();
	}
	}
	
	

