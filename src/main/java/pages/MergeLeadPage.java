package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{
			
		public MergeLeadPage() 
		{
			PageFactory.initElements(driver, this);
		}
		
		@FindBy (how = How.XPATH, using = "(//form[@name='MergePartyForm']/table/tbody/tr/td/a)[1]") WebElement FromIcon;
		@FindBy (how = How.XPATH, using = "(//form[@name='MergePartyForm']/table/tbody/tr/td/a)[2]") WebElement ToIcon;
		@FindBy (how = How.LINK_TEXT, using = "Merge") WebElement eleMergeButton;
		
		public FindLeadsPage clickFromIcon()
		{
			clickWithNoSnap(FromIcon);
			switchToWindow(1);
			return new FindLeadsPage();
		}
		
		public FindLeadsPage clickToIcon()
		{
			clickWithNoSnap(ToIcon);
			switchToWindow(1);
			return new FindLeadsPage();
		}
		
		public AlertPage clickMergeButton()
		{
			click(eleMergeButton);
			return new AlertPage();
		}
}

/*public MergeLeadPage()
{
	PageFactory.initElements(driver, this);
}

@FindBy(how = How.XPATH, using ="//form[@name='MergePartyForm']/table/tbody/tr/td/a)[1]") WebElement eleFrom;
@FindBy(how = How.XPATH,using="//form[@name='MergePartyForm']/table/tbody/tr/td/a)[2]") WebElement eleTo;

public FindLeads ClkFrom()
{
	click(eleFrom);
	switchToWindow(1);
	return new FindLeads();

}*/