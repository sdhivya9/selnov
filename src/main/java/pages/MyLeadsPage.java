package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
	
	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using ="Create Lead") WebElement eleCreate;
	@FindBy(how = How.LINK_TEXT, using ="MergeLead") WebElement eleMerge;
	
	public CreateLeadPage ClkCreateLead()
	{
		click(eleCreate);
		return new CreateLeadPage();
	}
	
	public MergeLeadPage clkMerge()
	{
		click(eleMerge);
		return new MergeLeadPage();
		
	}
	
	public FindLeadsPage clkFind()
	{
		click(eleMerge);
		return new FindLeadsPage();
		
	}
	
}
