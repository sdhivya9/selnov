package beginnerflipkart;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class BeginFlipkart {

	@Test
	public void flipkart() throws InterruptedException {
	
		//public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.flipkart.com");
		Thread.sleep(2000);
		//driver.switchTo().alert().dismiss();
		
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
		
		driver.findElementByXPath("//span[text() ='Electronics']").click();
		
		driver.findElementByXPath("//a[@title=\"Mi\"]").click();
		
		String title = driver.getTitle();
		if(title.equalsIgnoreCase("Mi Mobiles"))
			System.out.println(title +"Verified Successfully");
		
		driver.findElementByXPath("//div[text()='Newest First']").click();
		
		List <WebElement> allpdt = driver.findElementsByClassName("_3wU53n");
		for (WebElement each : allpdt) {
			System.out.println("Product name : " +each.getText());
		}
		
		List <WebElement> allprice = driver.findElementsByXPath("//*[@class=\"_6BWGkk\"]");
		for (WebElement each : allprice) {
			System.out.println("Price : " +each.getText());
		}
		
		
		WebElement pdttext = driver.findElementByXPath("(//div[@class=\"_3wU53n\"])[1]");
		String firstpdt = pdttext.getText();
		pdttext.click();
	   
		Set<String> allWindowHandles = driver.getWindowHandles();
		List<String> allhandles = new ArrayList<String>(allWindowHandles);
		driver.switchTo().window(allhandles.get(1));
		
	   String pgmatch = driver.getTitle();
	   if(pgmatch.contains(firstpdt))
	   System.out.println("Title is matched based on the product clicked");
	   
	   WebElement rating = driver.findElementByXPath("//span[text()[contains(.,'Ratings')]]");
		  System.out.println("Rating is : " +rating.getText());
	  
		  WebElement review = driver.findElementByXPath("//span[text()[contains(.,'Reviews')]]");
		System.out.println("Review is : " +review.getText());
		
		driver.quit();
		
	}

}
