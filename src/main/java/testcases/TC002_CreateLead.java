package testcases;

	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import pages.LoginPage;
	import wdMethods.ProjectMethods;

	public class TC002_CreateLead  extends ProjectMethods{
		@BeforeTest
		public void setData() {
			testCaseName = "TC002_CreateLead";
			testDescription = "CreateLead";
			authors = "Dhivya";
			category = "smoke";
			dataSheetName = "TC002";
			testNodes = "Leads";
		}
		
		@Test(dataProvider = "fetchData")
		public void login(String userName,String password, String Cname1, String Fname1, String Lname1) {		
			new LoginPage()
			.enterUserName(userName)
			.enterPassword(password)
			.clickLogin()
			.clickText()
			.clickLead()
			.ClkCreateLead()
			.Cname(Cname1)
			.Fname(Fname1)
			.Lname(Lname1)
			.CleadSub();
					
			/*LoginPage lp = new LoginPage();
			lp.enterUserName();
			lp.enterPassword();
			lp.clickLogin();*/
		}
		
	}



