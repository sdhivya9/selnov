package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods {
	
	@Before
	public void beforeSteps(Scenario sc)
	{
		startResult();
		startTestModule(sc.getName(), sc.getId());
		test = startTestCase(sc.getName());
		test.assignCategory("Functional");
		test.assignAuthor("Dhivya");
		startApp("chrome", "http://leaftaps.com/opentaps");
	}
	
	@After
	public void afterSteps(Scenario sc)
	{
		closeAllBrowsers();
		endResult();
	}
}

	/*import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{

	@Before
	public void before(Scenario sc)
	{
		System.out.println(sc.getName());
		System.out.println(sc.getId());
	}
	
	@After
	public void after(Scenario sc)
	{
		System.out.println(sc.getStatus());
		System.out.println(sc.isFailed());
	}
}

*/