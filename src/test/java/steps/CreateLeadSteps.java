package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadSteps {

	public ChromeDriver driver;
	
	@Given("Open the Browser")
	public void openBrowser()
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@Given("Max the browser")
	public void maxTheBrowser() {
	    driver.manage().window().maximize();
	}

	@Given("Set the timeout")
	public void setTheTimeout() {
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	   
	}

	@Given("Launch the URL")
	public void launchTheURL() {
		driver.get("http://leaftaps.com/opentaps/");
	}

	@Given("Enter the UserName as (.*)")
	public void enterTheUserNameAsDemoSalesManager(String data) {
		driver.findElementById("username").sendKeys(data);
	}

	@Given("Enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
		driver.findElementById("password").sendKeys(pwd);
	}

	@Given("Click on the Login button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click on CRM\\/SFA link")
	public void clickOnCRMSFALink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Click on Leads link")
	public void clickOnLeadsLink() {
		driver.findElementByLinkText("Leads").click();
	}

	@Given("Click on CreateLeads")
	public void clickOnCreateLeads() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter Company Name as (.*)")
	public void enterCompanyName(String cname) {
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	     
	}

	@Given("Enter First Name as (.*)")
	public void enterFirstName(String fname) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	     
	}

	@Given("Enter Last Name as (.*)")
	public void enterLastName(String lname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);	     
	}

	@When("Click on CreateLead button")
	public void clickOnCreateLeadButton() {
		driver.findElementByClassName("smallSubmit").click();
	     
	}

	@Then("Verify the creation")
	public void verifyTheCreation() {
	    System.out.println("Verified successfully");
	     
	}


}
